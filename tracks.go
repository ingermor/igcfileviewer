package main

// innehold i track datalagringsstruktur - JSON
type Track struct {
	H_date string `json:"h_date"`
	Pilot string `json:"pilot"`
	Glider string `json:"glider"`
	Glider_id string `json:"glider_id"`
	//Track_length float32 `json:"track_length"`
	Id string `json:"id"`
}

// in-memory datalagringsstuktur
type TracksIMS struct {
	tracks map[string]Track
}

// funksjon for å intitiere ims datalagring
func (ims *TracksIMS) Init()  {
	ims.tracks = make(map[string]Track)
}

// funksjon for å legge til en ny track i in-mem lagringen
func (ims *TracksIMS) AddTrack(t Track) {
	ims.tracks[t.Id] = t
}

// funksjon for å telle antall tracks i in-mem lagringen
// returnerer antall
func (ims *TracksIMS) CountTracks() int {
	return len(ims.tracks)
}