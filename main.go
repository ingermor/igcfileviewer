package main

import (
	"google.golang.org/appengine"
	"net/http"
)

//global variabel
var ims TracksIMS

func main() {
	ims := TracksIMS{} // in-memory lagring
	ims.Init() // initier ims

	http.HandleFunc("/api", handlerMeta)
	http.HandleFunc("/api/igc", handlerIgc)
	appengine.Main() // starter server for mottak av requests
}
