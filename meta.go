package main

import (
	"encoding/json"
	"net/http"
)

type metaInfo struct {
	Uptime  string `json:"uptime"`
	Info    string `json:""`
	Version string `json:"version"`
}

func handlerMeta(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "content-type", "application/json")

	//må ender uptime - legge til duration
	m := metaInfo{"uptime", "Service for IGC tracks", "v1"}
	json.NewEncoder(w).Encode(m)
}
