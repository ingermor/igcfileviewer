package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"html/template"
	"net/http"
	"regexp"
)

func handlerIgc(w http.ResponseWriter, r *http.Request)  {
	switch r.Method {
	case http.MethodGet:
		temp, err := template.ParseFiles("html/submit.gtpl")
		if err != nil {
			http.Error(w, "Error processing template", http.StatusInternalServerError)
		}
		temp.Execute(w, nil)
		break
	case http.MethodPost:
		htcheck := r.PostFormValue("checkht")
		pattern := `^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[\w]+([\-\.]{1}[\w]+)*\.[a-z]{2,5}(\/.*)?\.igc$`
		res, err := regexp.MatchString(pattern, htcheck)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		if res {
			s := htcheck
			track, err := igc.ParseLocation(s)
			if err != nil {
				fmt.Errorf("Problem reading the track", err)
			}

			intC := ims.CountTracks()
			id := intC + 1

			var newT Track
			newT.H_date = track.Date.String()
			newT.Pilot = track.Pilot
			newT.Glider = track.GliderType
			newT.Glider_id = track.GliderID
			newT.Id = string(id)

			ims.Init()
			ims.AddTrack(newT)

			http.Header.Add(w.Header(), "content-type", "application/json")
			json.NewEncoder(w).Encode(newT.Id)
			return
		}
		fmt.Fprintln(w, "Invalid URL")
		break
	default:
		http.Error(w, "Not implemented yet", http.StatusNotImplemented)
	}

}